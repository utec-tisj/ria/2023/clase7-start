import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-ejemplos',
  templateUrl: './ejemplos.component.html',
  styleUrls: ['./ejemplos.component.scss']
})
export class EjemplosComponent {

  public values: string = '';
  public nombre: string = '';
  public name = new FormControl('');

  onKey(event: any) { // without type info
    this.values = event.target.value;
  }
}
