import { Component } from '@angular/core';
import { Hospital } from '../models/hospital';
import { HospitalesService } from '../services/hospitales.service';

@Component({
  selector: 'app-hospitales',
  templateUrl: './hospitales.component.html',
  styleUrls: ['./hospitales.component.scss']
})
export class HospitalesComponent {

  public lista: Hospital[] = [];

  constructor(private service: HospitalesService) { }

  ngOnInit() {
    this.service.getHospitals().subscribe({
      next: (hospitals) => {
        this.lista = hospitals;
      },
      error: (error) => {
        console.log(error);
      },
      complete: () => {
        console.log('complete');
      }
    });
  }
}
