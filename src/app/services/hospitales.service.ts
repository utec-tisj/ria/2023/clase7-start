import { Injectable } from '@angular/core';
import { delay, Observable, of } from 'rxjs';
import { Hospital } from '../models/hospital';

@Injectable({
  providedIn: 'root'
})
export class HospitalesService {

  constructor() { }

      // Función para obtener un delay aleatorio entre 2 y 5 segundos
      private getRandomDelay(): number {
        return Math.floor(Math.random() * (5000 - 2000 + 1)) + 2000;
      }
   
      // Función para emular la llamada a una API que devuelve una colección de hospitales
      getHospitals(): Observable<Hospital[]> {
        // Datos de ejemplo de hospitales
        const hospitals: Hospital[] = [
          { id: 1, name: 'Hospital A' },
          { id: 2, name: 'Hospital B' },
          { id: 3, name: 'Hospital C' },
        ];
   
        // Retorna los datos de ejemplo con un delay aleatorio
        return of(hospitals).pipe(delay(this.getRandomDelay()));
      }
}
