import { Injectable } from '@angular/core';
import { delay, Observable, of } from 'rxjs';
import { Comida } from '../models/comida';

@Injectable({
  providedIn: 'root'
})
export class ComidasService {

  private lista: Comida[] = [
    { id: 1, name: 'Canelones' },
    { id: 2, name: 'Pollo al Horno' },
    { id: 3, name: 'Gnocchis' },
  ];

  constructor() { }

  // Función para obtener un delay aleatorio entre 2 y 5 segundos
  private getRandomDelay(): number {
    return Math.floor(Math.random() * (5000 - 2000 + 1)) + 2000;
  }

  // Función para emular la llamada a una API que devuelve una colección de comidas
  getComidas(): Observable<Comida[]> {
    // Retorna los datos de ejemplo con un delay aleatorio
    return of(this.lista).pipe(delay(this.getRandomDelay()));
  }

  // Función para emular la llamada a una API que añade una comida
  addComida(comida: Comida): Observable<Comida> {
    // Genera un nuevo id para la comida basado en el último elemento del array
    const newId = this.lista.length > 0 ? this.lista[this.lista.length - 1].id + 1 : 1;
    comida.id = newId;

    // Añade la comida al array
    this.lista.push(comida);

    // Retorna la nueva comida como un observable con un delay aleatorio
    return of(comida).pipe(delay(this.getRandomDelay()));
  }
}
