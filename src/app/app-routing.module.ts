import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EjemplosComponent } from './ejemplos/ejemplos.component';
import { HospitalesComponent } from './hospitales/hospitales.component';
import { TestComponent } from './test/test.component';

const routes: Routes = [
  {
    path:'test',
    component: TestComponent
  },
  {
    path: 'hospitales',
    component: HospitalesComponent
  },
  {
    path: 'ejemplos',
    component: EjemplosComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
